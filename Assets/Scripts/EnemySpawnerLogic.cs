﻿using UnityEngine;
using System.Collections;

public class EnemySpawnerLogic : MonoBehaviour {
	public GameObject m_enemy;
	public const int MaxEnemies = 25;
	private int m_enemyCount = 0;

	private bool m_spawnTimerActive = false;
	private float m_spawnTimer = 0.0f;
	private const float MinTimeBetweenSpawnsInSeconds = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!m_spawnTimerActive) {
			m_spawnTimerActive = TrySpawnEnemy ();
		} else {
			m_spawnTimer += Time.deltaTime;

			if (m_spawnTimer > MinTimeBetweenSpawnsInSeconds)
			{
				m_spawnTimerActive = false;
				m_spawnTimer = 0.0f;
			}

		}
	}

	bool TrySpawnEnemy()
	{
		if (Random.value > 0.97f) {

			EnemyController controller = m_enemy.gameObject.GetComponent<EnemyController>();
			controller.m_movementSpeed = Random.Range(0.5f, 2.5f);

			System.Array array = System.Enum.GetValues(typeof(AttackZoneLogic.AttackRange));
			AttackZoneLogic.AttackRange attackRange = (AttackZoneLogic.AttackRange)array.GetValue(UnityEngine.Random.Range(1,array.Length));

			controller.m_attackRange = attackRange;

			Quaternion quat = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
			Instantiate(m_enemy, transform.position, quat);
			++m_enemyCount;
			return true;
		}

		return false;
	}

	
}
