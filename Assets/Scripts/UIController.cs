﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	private PlayerInfo m_playerInfo;
	private GameObject m_breathMeter;
	private Vector3 m_breathMeterPosition;
	private float m_totalBreathMeterLength;

	public Text m_scoreText;

	// Use this for initialization
	void Start () {
		m_breathMeter = GameObject.Find ("Breath Meter");
		m_breathMeterPosition = m_breathMeter.transform.localPosition;
		m_playerInfo = GameObject.Find ("PlayerInfo").GetComponent<PlayerInfo> ();
		m_totalBreathMeterLength = 2.0f;

	}
	
	// Update is called once per frame
	void Update () {

		var currentScale = m_breathMeter.transform.localScale;

		var breathAmount = m_playerInfo.GetBreathAmount();

		Debug.Log ("BreathAmount is " + breathAmount);
		currentScale.x = breathAmount;

		m_breathMeter.transform.localScale = currentScale;

		var breathMeterPosition = m_breathMeterPosition;;

		var normalizedBreathAmount = breathAmount / 100.0f;
		var offset = (normalizedBreathAmount * m_totalBreathMeterLength) * 0.5f;
		breathMeterPosition.x += offset;
		m_breathMeter.transform.position = breathMeterPosition;

		UpdateScoreText ();
	}

	void UpdateScoreText(){
		m_scoreText.text = "Score: " + m_playerInfo.GetScore ();
	}
}
