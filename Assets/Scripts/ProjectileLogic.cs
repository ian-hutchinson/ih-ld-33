﻿using UnityEngine;
using System.Collections;

public class ProjectileLogic : MonoBehaviour {

	public GameObject m_blood;
	private GameObject m_monster;
	private bool m_fired = false;
	private float m_speed;
	private bool m_embedded = false;
	private GameObject m_objectEmbedded;
	private PlayerInfo m_playerInfo;
	public float m_damage;

	private float m_killTimer = 0.0f;
	private const float TimeToKill = 10.0f;
	private const float FadeTimeInSeconds = 5.0f;
	private bool m_fading = false;

	// Use this for initialization
	void Start () {
		m_damage = 10.0f;
		m_monster = GameObject.Find ("MonsterHead");
		m_playerInfo = GameObject.Find ("PlayerInfo").GetComponent<PlayerInfo> ();


	}
	
	// Update is called once per frame
	void Update () {
		if (m_fired) {
//			Rigidbody2D rigidBody = gameObject.GetComponent<Rigidbody2D>();
//			transform.rotation.SetLookRotation(rigidBody.velocity); 
			m_killTimer += Time.deltaTime;

			var controller = m_monster.GetComponent<MonsterHeadController>();
			if (m_killTimer > TimeToKill || (m_embedded && controller.IsShielding()))
			{
				BeginFadeOut();
			}
		}

		if (m_fading) {
			var sr = gameObject.GetComponent<SpriteRenderer>();
			var color = sr.color;
			color.a -= FadeTimeInSeconds * Time.deltaTime;
			sr.color = color;

			if (sr.color.a <= 0.0f){
				Destroy (gameObject);
			}
		}

		if (m_embedded) {
			Quaternion quat = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
//
			Vector3 toEmbedded = m_objectEmbedded.transform.position - transform.position;
			Instantiate(m_blood, transform.position + (toEmbedded * toEmbedded.magnitude * 0.5f), quat);
		}
	}

	void BeginFadeOut()
	{
		m_fired = false;
		m_fading = true;
	}

	public void SetFired(bool fired)
	{
		m_fired = fired;

		if (m_fired) {
			transform.SetParent(null);
			Rigidbody2D rigidBody = gameObject.GetComponent<Rigidbody2D>();
			rigidBody.isKinematic = false;

			Vector2 throwPoint = m_monster.transform.position - transform.position;
			float distance = throwPoint.magnitude;
			throwPoint.Normalize();

			rigidBody.AddForce (throwPoint * 4.5f, ForceMode2D.Impulse);
	
		}
	}

	public void SetObjectEmbedded(GameObject go)
	{
		m_objectEmbedded = go;
		m_embedded = true;

		m_playerInfo.SubtractHealth (m_damage);
	}

	public void SetSpeed(float speed)
	{
		m_speed = speed;
	}

}
