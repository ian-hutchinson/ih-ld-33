﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour {
	public enum EnemyState
	{
		Moving,
		Aiming,
		Attacking
	};

	public float m_movementSpeed = 0.2f;
	private bool m_isInAttackZone = false;
	private EnemyState m_enemyState;
	public AttackZoneLogic.AttackRange m_attackRange;
	private GameObject m_monster;

	public float m_health;

	public GameObject m_projectile;
	private float m_currentRotation = 0.0f;
	private float m_targetRotation;

	bool m_mustAttack = false;

	private PlayerInfo m_playerInfo;

	// Use this for initialization
	void Start () {
		m_health = 10.0f;

		m_monster = GameObject.Find ("MonsterHead");

		m_enemyState = EnemyState.Moving;

		Quaternion quat = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
		m_projectile = (GameObject) Instantiate(m_projectile, transform.position, quat);
		m_projectile.transform.SetParent (transform);
		m_projectile.transform.localPosition = new Vector3 (0.0f, 0.0f, 0.0f);

		m_playerInfo = GameObject.Find ("PlayerInfo").GetComponent<PlayerInfo> ();

//		m_projectile.transform.Rotate (Vector3.forward, projectileAngle);
	}
	
	// Update is called once per frame
	void Update () {

		if (m_health <= 0.0f) {
			m_playerInfo.AddToScore(1);
			Destroy (gameObject);
		}

		if (m_mustAttack) {
			StartAiming();
			m_mustAttack = false;
		}

		switch (m_enemyState) {

		case EnemyState.Moving:
			if (m_isInAttackZone)
			{
				RandomlyAttack();
			}
			else
			{
				MoveForward();
			}
			break;

		case EnemyState.Aiming:

			// Move rotation to target
			// WHen complete, attack
			m_currentRotation += 0.5f;
			m_projectile.transform.Rotate (Vector3.forward, -0.5f);

			if (m_currentRotation >= m_targetRotation)
			{
				Attack ();
			}
			break;

		case EnemyState.Attacking:
			MoveForward();
			break;
		}
	}

	void MoveForward()
	{
		Vector3 movement = new Vector3(-m_movementSpeed * Time.deltaTime, 0.0f, 0.0f);
		transform.Translate(movement);
	}

	void RandomlyAttack()
	{
		if (Random.value > 0.99) {
			StartAiming();
		} else {
			MoveForward();
		}
	}

	void StartAiming()
	{
		m_enemyState = EnemyState.Aiming;

		Vector2 forwardDirection = new Vector2 (-1.0f, 0.0f);
		Vector2 toMonster = m_monster.transform.position - transform.position;
		toMonster.Normalize ();

		float dotTheta = Vector2.Dot (forwardDirection, toMonster);
		float targetRotation = Mathf.Acos (dotTheta);

		float targetRotationDegrees = targetRotation * Mathf.Rad2Deg;

		m_targetRotation = targetRotationDegrees;
	}

	void Attack()
	{
		m_enemyState = EnemyState.Attacking;

		ProjectileLogic projectileLogic = m_projectile.GetComponent<ProjectileLogic> ();
		projectileLogic.SetFired (true);

		float projectileSpeed = Random.Range (5.0f, 10.0f);
		projectileLogic.SetSpeed (projectileSpeed);

		m_movementSpeed *= -1;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.name.Contains ("AttackZone")) {
			AttackZoneLogic zoneLogic = col.gameObject.GetComponent<AttackZoneLogic> ();

			if (zoneLogic.m_attackRange == m_attackRange) {
				Debug.Log ("In attack zone");
				m_isInAttackZone = true;
			}

			if (zoneLogic.m_attackRange == AttackZoneLogic.AttackRange.Short) {
				m_mustAttack = true;
			}
		} else if (col.gameObject.name.Contains ("EnemyKiller")) {
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.name.Contains ("Fire")) {
			m_health -= 0.5f;
		}
	}
}
