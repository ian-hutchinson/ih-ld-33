﻿using UnityEngine;
using System.Collections;

public class PlayerInfo : MonoBehaviour {

	private float m_health = 30.0f;
	private float m_breath = 100.0f;
	private const float MaxBreath = 100.0f;
	private float m_breathRechargeRate = 30.0f;
	private int m_score = 0;

	const float TimeUntilGameOver = 3.0f;
	float m_deathTimer = 0.0f;
	bool m_isDead = false;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (m_isDead) {
			m_deathTimer += Time.deltaTime;

			if (m_deathTimer >= TimeUntilGameOver)
			{
				Application.LoadLevel("GameOverScene");
			}
		}

		if (m_health <= 0.0f && !m_isDead) {
			m_isDead = true;
			PlayerPrefs.SetInt("SCORE", m_score);

			GameObject[] objectsToDestroy = GameObject.FindGameObjectsWithTag ("NeckTag");

//			foreach (var obj in objectsToDestroy) {
//				var hingeJoint = obj.GetComponent<HingeJoint2D>();
//				hingeJoint.connectedBody = null;
//			}

			var topNeck = GameObject.Find ("NeckLink 10");
			var neckHinge = topNeck.GetComponent<HingeJoint2D> ();
			Component.Destroy (neckHinge);

			var body = GameObject.Find ("MonsterBody");
			var bodyRb = body.GetComponent<Rigidbody2D> ();
			bodyRb.isKinematic = false;
			bodyRb.mass = 100.0f;

			var head = GameObject.Find ("MonsterHead");
			var headRb = head.GetComponent<Rigidbody2D> ();
			headRb.gravityScale = 1.0f;
			headRb.mass = 100000.0f;

			var headController = head.GetComponent<MonsterHeadController> ();
			headController.SetInputEnabled (false);
		} else {
			m_breath += m_breathRechargeRate * Time.deltaTime;

			m_breath = Mathf.Clamp(m_breath, 0.0f, MaxBreath);

		}
	}

	public bool HasBreath() {
		return m_breath > 10.0f;
	}

	public void SubtractBreath(float amount) {
		m_breath -= amount;
	}

	public void SubtractHealth(float amountToSubtract){
		m_health -= amountToSubtract;
	}

	public float GetBreathAmount()
	{
		return m_breath;
	}

	public void AddToScore(int amount)
	{
		m_score += amount;
	}

	public int GetScore()
	{
		return m_score;
	}
}
