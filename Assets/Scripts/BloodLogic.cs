﻿using UnityEngine;
using System.Collections;

public class BloodLogic : MonoBehaviour {

	private float m_fallSpeed;
	private float m_horiztonalSpeed;
	float m_deathTimer; 
	float m_elapsed = 0.0f;
	private bool m_active;

	// Use this for initialization
	void Start () {
		transform.localScale = new Vector3(1.0f, 1.0f, 1.0f) * Random.Range (1.5f, 3.0f);
		m_fallSpeed = Random.Range (2.0f, 8.0f);
		m_horiztonalSpeed = Random.Range (-1.0f, 1.0f);
		m_deathTimer = Random.Range (3.0f, 6.0f);
	}

	// Update is called once per frame
	void Update () {
		transform.Translate (new Vector3(0.0f, -1.0f, 1.0f) * m_fallSpeed * Time.deltaTime);
		transform.Translate (new Vector3(1.0f, 0.0f, 1.0f) * m_horiztonalSpeed * Time.deltaTime);

		m_elapsed += Time.deltaTime;

		if (m_elapsed >= m_deathTimer) {
			SpriteRenderer sr = gameObject.GetComponent<SpriteRenderer>();
			Color color = sr.color;
			color.a -= 0.05f;

			sr.color = color;

			if (sr.color.a <= 0.0f){
				Destroy (gameObject);
			}
		}
	}

	public bool IsActive()
	{
		return m_active;
	}
}
