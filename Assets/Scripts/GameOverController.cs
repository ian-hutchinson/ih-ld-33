﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour {

	public Text m_scoreText;

	// Use this for initialization
	void Start () {
		m_scoreText.text = "GAME OVER\nYOUR SCORE WAS " 
			+ PlayerPrefs.GetInt ("SCORE") 
				+ "\nPRESS ANY KEY TO RESTART";
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown) {
			Application.LoadLevel("MainScene");
		}
	}
}
