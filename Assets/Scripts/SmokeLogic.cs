﻿using UnityEngine;
using System.Collections;

public class SmokeLogic : MonoBehaviour {
	
	public float m_speed;
	private float m_scaleSpeed;
	private float m_timer = 0.0f;
	private float m_deathTime;

	// Use this for initialization
	void Start () {
		m_scaleSpeed = Random.Range (4.0f, 7.0f);
		m_deathTime = Random.Range (4.0f, 8.0f);
		m_speed = Random.Range (2.0f, 5.0f);
	}
	
	// Update is called once per frame
	void Update () {
//		transform.RotateAround (transform.localPosition, Vector3.forward, 1.0f);
		transform.Translate (Vector3.up * m_speed * Time.deltaTime);

		transform.localScale += new Vector3(1.0f, 1.0f, 1.0f) * m_scaleSpeed * Time.deltaTime;
		
		m_timer += Time.deltaTime;
		
		if (m_timer >= m_deathTime) {
			Destroy(gameObject);
		}
	}
}
