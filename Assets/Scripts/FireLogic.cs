﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FireLogic : MonoBehaviour {

	public Vector3 m_forward;
	public float m_speed;
	private float m_scaleSpeed;
	private float m_timer = 0.0f;
	private float m_upTimer;
	private float m_deathTime;

	private List<string> m_typesToSpawnSmoke = new List<string>();

	public GameObject m_smokeObject;

	private bool m_shouldSpawnSmoke = false;

	// Use this for initialization
	void Start () {
		m_typesToSpawnSmoke.Add ("Enemy");
		m_typesToSpawnSmoke.Add ("Ground");

		m_scaleSpeed = Random.Range (3.0f, 5.0f);
		m_upTimer = Random.Range (1.0f, 2.5f);
		m_deathTime = Random.Range (2.0f, 4.0f);
		m_speed = Random.Range (4.0f, 6.0f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.localScale += new Vector3(1.0f, 1.0f, 1.0f) * m_scaleSpeed * Time.deltaTime;
		transform.Translate (m_forward * m_speed * Time.deltaTime);

		m_timer += Time.deltaTime;

		if (m_timer >= m_upTimer) {
			transform.Translate (new Vector3(0.0f, 1.0f, 0.0f) * 3.0f * Time.deltaTime);
		}

		if (m_timer >= m_deathTime) {
			Destroy(gameObject);
		}
	}

	public void SetSpawnSmoke(bool shouldSpawnSmoke)
	{
		m_shouldSpawnSmoke = shouldSpawnSmoke;
	}

	void OnCollisionEnter2D(Collision2D col){

		foreach (var objectType in m_typesToSpawnSmoke) {
			if (col.gameObject.name.Contains (objectType)) {
				m_shouldSpawnSmoke = true;
				Destroy (gameObject);
			}
		}

	}

	void OnDestroy()
	{
		if (m_shouldSpawnSmoke) {
			Quaternion quat = new Quaternion (0.0f, 0.0f, 0.0f, 0.0f);
			Instantiate (m_smokeObject, transform.position, quat);
		}
	}

}
