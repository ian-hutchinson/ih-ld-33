﻿using UnityEngine;
using System.Collections;

public class MonsterHeadController : MonoBehaviour {
	
	private float speed = 10.0f;
	private float m_rotationSpeed = 5.0f;
	private bool m_firing = false;

	private bool m_inputEnabled = true;

	public GameObject m_projectile;

	private PlayerInfo m_playerInfo;

	bool m_shielding = false;

	// Use this for initialization
	void Start () {
	 	m_playerInfo = GameObject.Find ("PlayerInfo").GetComponent<PlayerInfo> ();
	}
	
	// Update is called once per frame
	void Update () {


		if (m_inputEnabled) {

			m_shielding = false;

			if (Input.GetKey(KeyCode.J)){
				Shield();
			} else if (Input.GetKey (KeyCode.L)) {
				transform.Rotate (Vector3.forward * -m_rotationSpeed);
			} else if (Input.GetKey (KeyCode.K)) {
				transform.Rotate (Vector3.forward * m_rotationSpeed);
			}

			if (!m_shielding) {
				Vector3 move = new Vector3 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"), 0);
				transform.position += move * speed * Time.deltaTime;

				if (Input.GetKey (KeyCode.Space)) {
					if (m_playerInfo.HasBreath()){
						Fire ();
					}
				}

			}

			UpdateNeck();
		}
	}

	void Shield()
	{
		m_shielding = true;
	}

	void UpdateNeck()
	{
		Color shieldingColor;
		float mass;

		var headRb = gameObject.GetComponent<Rigidbody2D> ();

		if (m_shielding) {
			shieldingColor = new Color (0.2f, 0.2f, 1.0f, 1.0f);
			mass = 500.0f;
		} else {
			shieldingColor = new Color (1.0f, 1.0f, 1.0f, 1.0f);
			mass = 0.05f;
		}
		
		GameObject[] neckLinks = GameObject.FindGameObjectsWithTag("NeckTag");
		
		foreach (var neckLink in neckLinks){
			var sr = neckLink.GetComponent<SpriteRenderer>();
			sr.color = shieldingColor;

			var rb = neckLink.GetComponent<Rigidbody2D>();
			rb.mass = mass;

//			if (m_shielding){
				rb.velocity = Vector2.zero;
//			}
		}
	}

	void Fire()
	{
		FireLogic fireLogic = m_projectile.gameObject.GetComponent<FireLogic> ();
		Vector3 projectileDirection = transform.up;

		float rotationAngle = -90 + Random.Range (-3.0f, 3.0f);

		projectileDirection = Quaternion.AngleAxis(rotationAngle, Vector3.forward) * projectileDirection;
		projectileDirection.Normalize ();
		fireLogic.m_forward = projectileDirection;

		Quaternion quat = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
		Instantiate(m_projectile, transform.position + (1.0f * projectileDirection) - (0.3f * transform.up), quat);

		m_playerInfo.SubtractBreath (2.0f);
	}

	public bool IsShielding()
	{
		return m_shielding;
	}

	public void SetInputEnabled(bool enabled)
	{
		m_inputEnabled = enabled;
	}

}
