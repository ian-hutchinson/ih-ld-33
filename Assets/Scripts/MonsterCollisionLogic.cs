using UnityEngine;
using System.Collections;

public class MonsterCollisionLogic : MonoBehaviour {

	private MonsterHeadController m_controller;

	// Use this for initialization
	void Start () {
		m_controller = GameObject.Find ("MonsterHead").GetComponent<MonsterHeadController> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.name.Contains ("Projectile")) {
			Rigidbody2D rb = col.gameObject.GetComponent<Rigidbody2D>();

			if (m_controller.IsShielding())
			{
				var toCol = col.transform.position - transform.position;
				rb.AddForce(toCol * 5.0f, ForceMode2D.Force);
				return;
			}

			if (col.gameObject.transform.rotation.z < 90.0f && rb.velocity.magnitude > 5.0f)
			{
				col.gameObject.transform.SetParent (transform);
				rb.isKinematic = true;		
				ProjectileLogic pl = col.gameObject.GetComponent<ProjectileLogic>();
				pl.SetObjectEmbedded(gameObject);
			}
		}
	}
}
